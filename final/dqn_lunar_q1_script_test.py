from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines.deepq.policies import MlpPolicy,LnMlpPolicy
from stable_baselines import DQN
from matplotlib import animation
import matplotlib.pyplot as plt
import gym

# Best model using hyperparameters:
# DQN(LnMlpPolicy, env, verbose=1,gamma=0.99,learning_rate=0.0010,batch_size=16)
# With a total timesteps:
# model.learn(total_timesteps=40000)

"""
For this function you need to install imagemagick with
sudo apt-get install imagemagick (Ubuntu)
sudo pacman -Sy imagemagick (Manjaro)
"""
def create_gif(frames, path="./final/",filename="animation.gif"):
    plt.figure(
        figsize=(
            frames[0].shape[1] / 72.0, frames[0].shape[0] / 72.0),
        dpi=72
        
    )

    patch = plt.imshow(frames[0])
    plt.axis('off')

    def animate(i):
        patch.set_data(frames[i])

    anim = animation.FuncAnimation(
        plt.gcf(),
        animate,
        frames=len(frames),
        interval=50
    )
    anim.save(path+filename,writer="imagemagick",fps=60)


env = gym.make('LunarLander-v2')
env = DummyVecEnv([lambda:env])

model = DQN.load("./final/dqn_lunar_q1")

obs = env.reset()
frames = []

while True:
    action, _states = model.predict(obs)
    obs, rewards, done, info = env.step(action)
    if done:
        break
    frames.append(env.render(mode="rgb_array"))

create_gif(frames)