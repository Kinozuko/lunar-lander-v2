# lunar-lander-v2

![](final/dqn_lunar_q1.gif)

Lunar Lander OpenIA problem with DQN model

In this problem we will use the gym library from Open IA in a Manjaro system using python 3.7.4 and tensorflow 1.15.0 and [DQN Deep Q Network](https://stable-baselines.readthedocs.io/en/master/modules/dqn.html)

We do some changes to the [Lunar Lander](https://github.com/openai/gym) problem like changing the INITIAL_RANDOM to 1500.0.

To successul run program you should install

# Ubuntu 16.04 and 18.04
```
apt-get install -y libglu1-mesa-dev libgl1-mesa-dev libosmesa6-dev xvfb ffmpeg curl patchelf libglfw3 libglfw3-dev cmake zlib1g zlib1g-dev swig

apt-get install imagemagick 

```

# Manjaro 21.0.7
```
pacman -Sy swig cmake openmpi zlib

pacman -Sy imagemagick
```

# Installing Open IA Gym and other libraries

- git clone https://github.com/openai/gym.git
- cd gym
- pip3.7 install -e .
- cd ..
- pip3.7 install -r requirements.txt

# Executing

- python3.7 script/dqn_lunar_q1_script.py

In this execution it will train a new model and create a file called *dqn_lunar.zip* with filepath *./script/dqn_lunar.zip*, feel free to change this script and their hyperparameters to test differents trainings.

# Testing

- python3.7 final/dqn_lunar_q1_script_test

In the folder *final* there's a script called *dqn_lunar_q1_script_test.py* and this will test the model in the file *dqn_lunar_q1.zip* and generate a gif animation called *animation.gif", also there's a file called *dqn_lunar_q1.gif* which is one of the running model. 

*DISCLAIMER: dqn_lunar_q1_script.py script doesn't train any model*

# About model

Hyperparameters:

DQN(LnMlpPolicy, env, verbose=1,gamma=0.99,learning_rate=0.0010,batch_size=16)

Timesteps:

model.learn(total_timesteps=40000)
