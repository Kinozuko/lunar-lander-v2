from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines.deepq.policies import MlpPolicy,LnMlpPolicy
from stable_baselines import DQN
import gym

env = gym.make('LunarLander-v2')
env = DummyVecEnv([lambda:env])

model = DQN(LnMlpPolicy, env, verbose=1,gamma=0.99,learning_rate=0.0005,batch_size=16)


model.learn(total_timesteps=50000)
model.save("./script/dqn_lunar")

del model

model = DQN.load("./script/dqn_lunar")

obs = env.reset()

while True:
    action, _states = model.predict(obs)
    obs, rewards, dones, info = env.step(action)
    env.render()